//
//  TestCollectionViewCell.swift
//  testCalendar
//
//  Created by MIS-MACMINI5 on 2020/6/16.
//  Copyright © 2020 tylerkuo. All rights reserved.
//

import UIKit
import SnapKit
class TestCollectionViewCell: JYCalendarCell {
    
    var dayLable : UILabel = UILabel();
    
    var imageView: UIImageView = UIImageView();
    
    override init(frame: CGRect) {
        super.init(frame: frame);
        self.onCreate();
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder);
        self.onCreate();
    }
    
    func onCreate(){
        self.contentView.addSubview(dayLable);
        
        imageView.contentMode = .scaleToFill;
        self.contentView.addSubview(imageView);
        
        dayLable.snp.makeConstraints { (make) in
            make.centerY.equalTo(self.contentView);
            make.centerX.equalTo(self.contentView);
        }
        
        imageView.snp.makeConstraints { (make) in
            make.edges.equalTo(self.contentView);
        }
    }
    
    func setDelDay(isDel: Bool){
        if isDel{
            imageView.image = UIImage.init(named: "calendar_del_line");
        }else{
            imageView.image = nil;
        }
    }
    
    
}
