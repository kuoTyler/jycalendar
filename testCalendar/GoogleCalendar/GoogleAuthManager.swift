//
//  GoogleAuthManager.swift
//  testCalendar
//
//  Created by baytony on 2018/12/17.
//  Copyright © 2018 tylerkuo. All rights reserved.
//

import UIKit
import GoogleSignIn




class GoogleAuthManager: NSObject, GIDSignInDelegate {

    var kClientID = "786125012904-t3qoqt2qhs3a1mmtb0aci3i3s7kf751d.apps.googleusercontent.com";
    
    var callback:(() -> Void)? = nil;

    var currentUser: GIDGoogleUser?{
        return GIDSignIn.sharedInstance()?.currentUser;
    }
    
    override init() {
        super.init();
        GIDSignIn.sharedInstance()?.delegate = self;
    }
    
    func login(success:@escaping () -> Void){
        GIDSignIn.sharedInstance()?.clientID = kClientID;
        GIDSignIn.sharedInstance()?.scopes = ["https://www.googleapis.com/auth/calendar.events"];
        GIDSignIn.sharedInstance()?.signIn();
        self.callback = success;
    }
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if let error = error{
            print("\(error.localizedDescription)");
        }else{
            let userId = user.userID;
            let idToken = user.authentication.idToken;
            let fullName = user.profile.name;
            let givenName = user.profile.givenName;
            let familyName = user.profile.familyName;
            let email = user.profile.email;
        }
        if (callback != nil){
            callback!();
        }
    }
    
    //MARK: - GIDSignInUIDelegate
    func sign(_ signIn: GIDSignIn!, present viewController: UIViewController!) {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else{
            return;
        }
        guard let view = appDelegate.window?.rootViewController else{
            return;
        }
        
        view.present(viewController, animated: true, completion: nil);
    }
    

}
