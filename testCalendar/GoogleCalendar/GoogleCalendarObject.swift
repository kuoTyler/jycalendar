//
//  GoogleCalendarObject.swift
//  testCalendar
//
//  Created by baytony on 2018/12/17.
//  Copyright © 2018 tylerkuo. All rights reserved.
//

import UIKit
import GoogleAPIClientForREST
class GoogleCalendarObject: NSObject {
    var m_id: String!;
    var m_created: Date!;
    var m_creatorDispalyName: String!;
    var m_creatorEmail: String!;
    var m_description: String!;
    var m_endDate: Date?;
    var m_startDate: Date?;
    var m_summary: String!;
    var m_calendarDisplayName: String!;
    var m_calendarId: String!;
    var m_isAllDay: Bool!;
    
    var startDate: String{
        if let date = m_startDate{
            let formatter = DateFormatter();
            formatter.dateFormat = "yyyyMMdd";
            formatter.timeZone = TimeZone(abbreviation: "UTC");
            let utcTimeZoneStr = formatter.string(from: date);
            
            return utcTimeZoneStr;
        }
        return "";
    }
    var endDate: String{
        if let date = m_endDate{
            let formatter = DateFormatter();
            formatter.dateFormat = "yyyyMMdd";
            formatter.timeZone = TimeZone(abbreviation: "UTC");
            let utcTimeZoneStr = formatter.string(from: date);
            
            return utcTimeZoneStr;
        }
        return "";
    }
    
    var key: String{
        return startDate;
    }
    
    override init() {
        super.init();
        m_id = "";
        self.m_created = Date();
        self.m_creatorDispalyName = "";
        self.m_creatorEmail = "";
        self.m_description = "";
        self.m_endDate = Date();
        self.m_startDate = Date();
        self.m_summary = "";
        self.m_calendarDisplayName = "";
        self.m_calendarId = "";
        self.m_isAllDay = true;
    }
    
    convenience init(calendarEvent: GTLRCalendar_Event) {
        self.init();
        
        if let created = calendarEvent.created?.date{
            self.m_created = created;
        }
        if let dispalyName = calendarEvent.creator?.displayName{
            self.m_creatorDispalyName = dispalyName;
        }
        if let calendarId = calendarEvent.organizer?.email{
            self.m_calendarId = calendarId;
        }
        if let calendarDispalyName = calendarEvent.organizer?.displayName{
            self.m_calendarDisplayName = calendarDispalyName;
        }
        if let email = calendarEvent.creator?.email{
            self.m_creatorEmail = email;
        }
        if let description = calendarEvent.descriptionProperty{
            self.m_description = description;
        }
        if let endDate = calendarEvent.end?.date?.date{
            let calendar = Calendar.current;
            self.m_endDate = calendar.date(byAdding: .second, value: -3600*12-1, to: endDate);
        }else if let endTime = calendarEvent.end?.dateTime?.date{
            self.m_endDate = endTime;
            self.m_isAllDay = false;
        }else{
            self.m_endDate = nil;
        }
        if let startDate = calendarEvent.start?.date?.date{
            let calendar = Calendar.current;
            self.m_startDate = calendar.date(byAdding: .second, value: -3600*12, to: startDate);
        }else if let startTime = calendarEvent.start?.dateTime?.date{
            self.m_startDate = startTime;
            self.m_isAllDay = false;
        }else{
            self.m_startDate = nil;
        }
        if let summary = calendarEvent.summary{
            self.m_summary = summary;
        }
        if let id = calendarEvent.identifier{
            self.m_id = id;
        }
    }
}
