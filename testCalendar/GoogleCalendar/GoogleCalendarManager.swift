//
//  GoogleCalendarManager.swift
//  testCalendar
//
//  Created by baytony on 2018/12/17.
//  Copyright © 2018 tylerkuo. All rights reserved.
//

import UIKit
import GoogleSignIn
import GoogleAPIClientForREST

class GoogleCalendarManager: NSObject {
    
    fileprivate lazy var calendarService: GTLRCalendarService? = {
        let service = GTLRCalendarService()
        // Have the service object set tickets to fetch consecutive pages
        // of the feed so we do not need to manually fetch them
        service.shouldFetchNextPages = true
        // Have the service object set tickets to retry temporary error conditions
        // automatically
        service.isRetryEnabled = true
        service.maxRetryInterval = 15
        
        guard let currentUser = GIDSignIn.sharedInstance().currentUser,
            let authentication = currentUser.authentication else {
                return nil
        }

        service.authorizer = authentication.fetcherAuthorizer()
        return service
    }();
    
    
    func getCalendarEvenList(startDateTime: Date, endDateTime:Date, callback: @escaping ([GoogleCalendarObject], Error?) -> Void){
        if (GIDSignIn.sharedInstance().currentUser == nil){
            return;
        }
        
        guard let service = self.calendarService else {
            return;
        }
        
        let eventsListQuery = GTLRCalendarQuery_EventsList.query(withCalendarId: kCalendarId);
        eventsListQuery.timeMin =  GTLRDateTime(date: startDateTime);
        eventsListQuery.timeMax = GTLRDateTime(date: endDateTime);
        
        _ = service.executeQuery(eventsListQuery, completionHandler: { (ticket, result, error) in
            guard error == nil, let items = (result as? GTLRCalendar_Events)?.items else{
                print("error: \(String(describing: error))");
                print("userInfo: \(String(describing: error?.localizedDescription))");
                callback([], error);
                return;
            }
            
            if items.count > 0{
                var list:[GoogleCalendarObject] = [];
                for item in items{
                    let object = GoogleCalendarObject.init(calendarEvent: item);
                    list.append(object);
                }
                callback(list, nil);
            }else{
                callback([], nil);
            }
        })
    }
    
    private func getKey(date: Date) -> String{
        let formatter = DateFormatter();
        formatter.dateFormat = "yyyyMM";
        return formatter.string(from: date);
    }
    
    func addEvent(item: GoogleCalendarObject, callback:@escaping (GoogleCalendarObject?, Error?) -> Void){
        if (GIDSignIn.sharedInstance().currentUser == nil){
            return;
        }
        guard let service = self.calendarService else {
            return;
        }
        
        if let startDate = item.m_startDate, let endDate = item.m_endDate{
            let newEvent = GTLRCalendar_Event();
            newEvent.summary = item.m_summary;
            newEvent.descriptionProperty = item.m_description;
            
            newEvent.start = GTLRCalendar_EventDateTime();
            newEvent.start?.date = GTLRDateTime(forAllDayWith: startDate);
            
            newEvent.end = GTLRCalendar_EventDateTime();
            newEvent.end?.date = GTLRDateTime(forAllDayWith: endDate);
            
            let query = GTLRCalendarQuery_EventsInsert.query(withObject: newEvent, calendarId: kCalendarId);
            
            service.executeQuery(query) { (ticket, event, error) in
                guard error == nil, let tItem = event as? GTLRCalendar_Event else{
                    print("error: \(String(describing: error))");
                    print("userInfo: \(String(describing: error?.localizedDescription))");
                    callback(nil, error);
                    return;
                }
                let object = GoogleCalendarObject.init(calendarEvent: tItem)
                callback(object, nil);
            }
        }

    }
    
    func updateEvent(item: GoogleCalendarObject, callback:@escaping (GoogleCalendarObject?, Error?) -> Void){
        
        if (GIDSignIn.sharedInstance().currentUser == nil){
            return;
        }
        guard let service = self.calendarService else {
            return;
        }
        
        if let startDate = item.m_startDate, let endDate = item.m_endDate{
            let updateEvent = GTLRCalendar_Event();
            updateEvent.identifier = item.m_id;
            updateEvent.summary = item.m_summary;
            updateEvent.descriptionProperty = item.m_description;
            
            updateEvent.start = GTLRCalendar_EventDateTime();
            updateEvent.start?.date = GTLRDateTime(forAllDayWith: startDate);
            
            updateEvent.end = GTLRCalendar_EventDateTime();
            updateEvent.end?.date = GTLRDateTime(forAllDayWith: endDate);
            
            let query = GTLRCalendarQuery_EventsUpdate.query(withObject: updateEvent, calendarId: item.m_calendarId, eventId: item.m_id);
            
            service.executeQuery(query) { (ticket, event, error) in
                guard error == nil, let tItem = event as? GTLRCalendar_Event else{
                    print("error: \(String(describing: error))");
                    print("userInfo: \(String(describing: error?.localizedDescription))");
                    callback(nil, error);
                    return;
                }
                let eventObject = GoogleCalendarObject.init(calendarEvent: tItem);
                callback(eventObject, nil);
            }
        }
    }
    
    func removeEvent(item: GoogleCalendarObject, callback:@escaping (GoogleCalendarObject, Error?) -> Void){
        if (GIDSignIn.sharedInstance().currentUser == nil){
            return;
        }
        guard let service = self.calendarService else {
            return;
        }
        let query = GTLRCalendarQuery_EventsDelete.query(withCalendarId: item.m_calendarId, eventId: item.m_id);
        service.executeQuery(query) { (ticket, event, error) in
            if (error != nil) {
                print("error: \(String(describing: error))");
                print("userInfo: \(String(describing: error?.localizedDescription))");
                callback(item, error);
                return;
            }
            callback(item, nil);
        }
    }
}
