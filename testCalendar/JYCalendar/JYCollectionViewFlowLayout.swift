//
//  JYCollectionViewFlowLayout.swift
//  testCalendar
//
//  Created by MIS-MACMINI5 on 2020/6/11.
//  Copyright © 2020 tylerkuo. All rights reserved.
//

import UIKit

class JYCollectionViewFlowLayout: UICollectionViewFlowLayout {
    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        guard let attributes = super.layoutAttributesForElements(in: rect) else { return nil }
                
        for i in 1..<attributes.count {
            let current = attributes[i]
            let original = attributes[i-1].frame.maxX
            if original + current.frame.width <= collectionViewContentSize.width {
                current.frame.origin.x = original
            }
        }
        return attributes
    }
}
