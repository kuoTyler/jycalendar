//
//  JYCalendarCollection.swift
//  testCalendar
//
//  Created by baytony on 2018/12/11.
//  Copyright © 2018 tylerkuo. All rights reserved.
//

import UIKit

enum CellDayType{
    case empty;     //不顯示
    case past;      //過去的日期
    case allDay;    //全天不可選
    case partDay;   //半天可選
    case allCanDay; //全天可選
}

enum CellDayBgType{
    case Hide;      //隱藏
    case Round;     //被選中的日期，完整圓
    case Left;      //被選中的日期，左側圓角
    case Center;    //被選中的日期，不切圓角
    case Right;     //被選中的日期，右側圓角
}


protocol JYCalendarCollectionDelegate {
    func calendar(_ collectionView: JYCalendarCollection, didSelectItemAt calendarIndex: CalendarIndex);
}

protocol JYCalendarCollectionDataSource {
    func calendar(_ collectionView: JYCalendarCollection, cellForItemAt calendarIndex: CalendarIndex) -> JYCalendarCell;
    
    func calendar(_ collectionView: JYCalendarCollection, CellDayBgTypeForItemAt calendarIndex: CalendarIndex) -> CellDayBgType;
}

public class JYCalendarCollection: UICollectionView {

    internal var JYDelegate: JYCalendarCollectionDelegate?;
    internal var JYDataSource: JYCalendarCollectionDataSource?;
    
    private var currentYear = Calendar.current.component(.year, from: Date());
    private var currentMonth = Calendar.current.component(.month, from: Date());

    private var numberOfDaysInThisMonth:Int{
        let dateComponents = DateComponents(year: currentYear, month: currentMonth);
        let date = Calendar.current.date(from: dateComponents)!;
        let range = Calendar.current.range(of: .day, in: .month, for: date);
        return range?.count ?? 0;
    };
    
    private var numberOfDaysInLastMonth: Int{
        var tMonth = currentMonth - 1;
        var tYear = currentYear;
        if (tMonth <= 0){
            tMonth = 12;
            tYear = tYear - 1;
        }
        let dateComponents = DateComponents(year: tYear, month: tMonth);
        let date = Calendar.current.date(from: dateComponents)!;
        let range = Calendar.current.range(of: .day, in: .month, for: date);
        return range?.count ?? 0;
    }
    
    private var whatDayIsIt:Int{
        let dateComponents = DateComponents(year: currentYear, month: currentMonth);
        let date = Calendar.current.date(from: dateComponents)!;
        return Calendar.current.component(.weekday, from: date);
    };
    
    private var howManyItemsShouldIAdd: Int{
        return whatDayIsIt - 1;
    }
    
    convenience public init(frame: CGRect = CGRect.zero){
        let layout = JYCollectionViewFlowLayout();
//        let layout = UICollectionViewFlowLayout();
        layout.scrollDirection = .vertical;
        layout.minimumLineSpacing = 0;
        layout.minimumInteritemSpacing = 0;
        self.init(frame: frame, collectionViewLayout: layout);
    }
    
    override init(frame: CGRect, collectionViewLayout layout: UICollectionViewLayout) {
        
        super.init(frame: frame, collectionViewLayout: layout);
        self.onCreate();
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder);
        self.onCreate();
    }
    private func onCreate(){
        self.isPagingEnabled = true;
        self.backgroundColor = UIColor.white;
        self.delegate = self;
        self.dataSource = self;
        self.isScrollEnabled = false;
        self.showsVerticalScrollIndicator = false;
        self.showsHorizontalScrollIndicator = false;
        self.bounces = false;
    }
    
    public func setMonth(month: Int, year: Int){
        self.currentMonth = month;
        self.currentYear = year;
        self.reloadData();
    }
    
    public func getCurrent() -> (month: Int, year: Int){
        return (self.currentMonth, self.currentYear);
    }
    
    //MARK: - action
    private func getCalendarIndex(indexPath: IndexPath) -> CalendarIndex{
        if indexPath.row < howManyItemsShouldIAdd{
            let last = JYCalendarComponent.getLastMonth(month: self.currentMonth, year: self.currentYear);
            let day = numberOfDaysInLastMonth - howManyItemsShouldIAdd + indexPath.row + 1;
            return CalendarIndex(year: last.year, month: last.month, day: day, indexPath: indexPath, type: .lastMonth);
        }else if indexPath.row >= howManyItemsShouldIAdd + numberOfDaysInThisMonth {
            let next = JYCalendarComponent.getNextMonth(month: self.currentMonth, year: self.currentYear);
            let day = indexPath.row - (howManyItemsShouldIAdd + numberOfDaysInThisMonth) + 1;
            return CalendarIndex(year: next.year, month: next.month, day: day, indexPath: indexPath, type: .nextMonth);
        }else{
            let day = indexPath.row + 1 - howManyItemsShouldIAdd;
            return CalendarIndex(year: self.currentYear, month: self.currentMonth, day: day, indexPath: indexPath, type: .currentMonth);
        }
    }
}
//MARK: - UICollectionViewDataSource
extension JYCalendarCollection: UICollectionViewDataSource{
    public func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1;
    }
    
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 42;
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let index = self.getCalendarIndex(indexPath: indexPath);
        
        guard let cell = JYDataSource?.calendar(self, cellForItemAt: index) else {
            fatalError("dataSource non implement")
        }
        cell.layoutMargins = UIEdgeInsets.zero;
        if let bgType = JYDataSource?.calendar(self, CellDayBgTypeForItemAt: index){
            cell.setSelect(bgType: bgType);
        }
        return cell;
    }
}

//MARK: - UICollectionViewDelegate
extension JYCalendarCollection: UICollectionViewDelegate{
    public func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let index = self.getCalendarIndex(indexPath: indexPath);
        JYDelegate?.calendar(self, didSelectItemAt: index);
        
    }
}

//MARK: - UICollectionViewDelegateFlowLayout
extension JYCalendarCollection: UICollectionViewDelegateFlowLayout{
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = ((collectionView.frame.width) / 7.0);
        let height = collectionView.frame.height / 6.0;
        return CGSize(width: width, height: height);
    }
}

extension Date{
    static func isHoliday(year: Int, month: Int, currentDay: Int) -> Bool{
        let dateComponents = DateComponents(year: year, month: month, day: currentDay);
        let date = Calendar.current.date(from: dateComponents)!;
        let day = Calendar.current.component(.weekday, from: date);
        return day == 1 || day == 7;
    }
    
    static func isToday(year: Int, month: Int, currentDay: Int) -> Bool{
        let dateComponents = DateComponents(year: year, month: month, day: currentDay);
        let date = Calendar.current.date(from: dateComponents)!;
        return Calendar.current.isDateInToday(date);
    }
}
