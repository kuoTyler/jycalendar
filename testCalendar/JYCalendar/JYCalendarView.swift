//
//  JYCalendarView.swift
//  testCalendar
//
//  Created by baytony on 2018/12/5.
//  Copyright © 2018 tylerkuo. All rights reserved.
//

import UIKit

public protocol JYCalendarDelegete {
//    func calendarView(_ collectionView: JYCalendarCollection, didSelectItemAt calendarIndex: CalendarIndex);
    
    func calendarView(_ collectionView: JYCalendarCollection, canSelectItemAt calendarIndex: CalendarIndex) -> Bool;
}

extension JYCalendarDelegete{
    func calendarView(_ collectionView: JYCalendarCollection, didSelectItemAt calendarIndex: CalendarIndex){
        
    }
    
    func calendarView(_ collectionView: JYCalendarCollection, canSelectItemAt calendarIndex: CalendarIndex) -> Bool{
        return true;
    }
}

public protocol JYCalendarDataSource {
    func calendarView(_ collectionView: JYCalendarCollection, cellForItemAt calendarIndex: CalendarIndex) -> JYCalendarCell;
    
    func calendarView(_ collectionView: JYCalendarCollection, year: Int, month: Int);
}

public class JYCalendarView: UIView{

    private var currentYear = Calendar.current.component(.year, from: Date());
    private var currentMonth = Calendar.current.component(.month, from: Date());
    
    public var delegate: JYCalendarDelegete?;
    public var dataSource: JYCalendarDataSource?;
    
    public private(set) var leftIndex: CalendarIndex? = nil;
    public private(set) var rightIndex: CalendarIndex? = nil;
    
    private var oldConstraints = [NSLayoutConstraint]();
    
    private var months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var timeLabel: UILabel!;
    @IBOutlet weak var weekdayView: UIView!
    
    private var collectionViews: [JYCalendarCollection] = [];
    
    public private(set) var justCanSelectOneDay: Bool = false;
    
    static public func loadViewFromNib() -> JYCalendarView {
        let bundle = Bundle.init(for: JYCalendarView.classForCoder());
        let nib = UINib(nibName: "JYCalendar", bundle: bundle);
        let view = nib.instantiate(withOwner: self, options: nil).first as! JYCalendarView;
        return view
    }
    
    public override func awakeFromNib() {
        super.awakeFromNib();
        self.initScrollView();
        self.setUp();
    }
    
    public func reloadDate(){
        for collectionView in collectionViews {
            collectionView.reloadData();
        }
    }
    
    public func register(_ cellClass: AnyClass?, forCellWithReuseIdentifier identifier: String){
        for collectionView in collectionViews{
            collectionView.register(cellClass, forCellWithReuseIdentifier: identifier);
        }
    }
    
    private func initScrollView(){
        self.scrollView.showsHorizontalScrollIndicator = false;
        self.scrollView.showsVerticalScrollIndicator = false;
        self.scrollView.isPagingEnabled = true;
        self.scrollView.delegate = self;
        
        for _ in 0 ..< 3{
            let tCollectionView = JYCalendarCollection();
            tCollectionView.JYDelegate = self;
            tCollectionView.JYDataSource = self;
            tCollectionView.register(JYCalendarCell.self, forCellWithReuseIdentifier: "JYCalendarCell");
            collectionViews.append(tCollectionView);
            scrollView.addSubview(tCollectionView)
        }
        self.updateCollectionConstraints();
        self.updateCollectionMonth();
    }
    override public func layoutSubviews() {
        super.layoutSubviews();
//        self.scrollView.delegate = self;
        self.scrollView.contentOffset = CGPoint(x: self.frame.size.width, y: 0);
    }
    
    override public func layoutIfNeeded() {
        super.layoutIfNeeded();
        self.setUp();
    }
    private func setUp(){
//        self.coordinateSpace = 
        timeLabel.text = months[currentMonth - 1] + " \(currentYear)";
//        collectionView.reloadData();
    }
    
    public func setSelectType(oneDay: Bool){
        justCanSelectOneDay = oneDay;
    }
    
    func makeDate(year: Int, month: Int, day: Int) -> Date {
        var calendar = Calendar(identifier: .gregorian);
        calendar.timeZone = TimeZone(secondsFromGMT: 0)!
        let components = DateComponents(year: year, month: month, day: day);
        return calendar.date(from: components)!
    }
    private func updateScrollView(page: Int){
        if (page == 2){
            let tCollectionView = collectionViews.remove(at: 0);
            collectionViews.append(tCollectionView);
        }else if (page == 0){
            let tCollectionView = collectionViews.remove(at: 2);
            collectionViews.append(tCollectionView);
        }
        
        self.updateCollectionConstraints();
        self.scrollView.contentOffset = CGPoint(x: self.frame.size.width, y: 0);
    }
    private func updateCollectionConstraints(){
        var lastView: UIView? = nil;
        NSLayoutConstraint.deactivate(oldConstraints);
        for tCollectionView in collectionViews{
            tCollectionView.translatesAutoresizingMaskIntoConstraints = false;
            let top = NSLayoutConstraint(
                item: tCollectionView,
                attribute: .top,
                relatedBy: .equal,
                toItem: self.weekdayView,
                attribute: .bottom,
                multiplier: 1.0,
                constant: 0);
            top.isActive = true;
            oldConstraints.append(top);
            let bottom = NSLayoutConstraint(
                item: tCollectionView,
                attribute: .bottom,
                relatedBy: .equal,
                toItem: self,
                attribute: .bottom,
                multiplier: 1.0,
                constant: 0)
            bottom.isActive = true;
            oldConstraints.append(bottom);
            let height = NSLayoutConstraint(
                item: tCollectionView,
                attribute: .height,
                relatedBy: .equal,
                toItem: self.scrollView,
                attribute: .height,
                multiplier: 1.0,
                constant: 0);
            height.isActive = true;
            oldConstraints.append(height);
            let width = NSLayoutConstraint(
                item: tCollectionView,
                attribute: .width,
                relatedBy: .equal,
                toItem: self.scrollView,
                attribute: .width,
                multiplier: 1.0,
                constant: 0);
            width.isActive = true;
            oldConstraints.append(width);
            if (lastView != nil){
                let leading = NSLayoutConstraint(
                    item: tCollectionView,
                    attribute: .leading,
                    relatedBy: .equal,
                    toItem: lastView,
                    attribute: .trailing,
                    multiplier: 1.0,
                    constant: 0);
                leading.isActive = true;
                oldConstraints.append(leading);
            }else{
                let leading = NSLayoutConstraint(
                    item: tCollectionView,
                    attribute: .left,
                    relatedBy: .equal,
                    toItem: self.scrollView,
                    attribute: .left,
                    multiplier: 1.0,
                    constant: 0);
                leading.isActive = true;
                oldConstraints.append(leading);
            }
            if collectionViews.last == tCollectionView{
                let trailing = NSLayoutConstraint(
                    item: tCollectionView,
                    attribute: .right,
                    relatedBy: .equal,
                    toItem: self.scrollView,
                    attribute: .right,
                    multiplier: 1.0,
                    constant: 0);
                trailing.isActive = true;
                oldConstraints.append(trailing);
            }
            lastView = tCollectionView;
        }
    }
   private func updateCollectionMonth(){
        for tCollectionView in collectionViews{
            if tCollectionView == collectionViews.first{
                let last = JYCalendarComponent.getLastMonth(month: self.currentMonth, year: self.currentYear);
                tCollectionView.setMonth(month: last.month, year: last.year);
            }else if tCollectionView == collectionViews.last{
                let next = JYCalendarComponent.getNextMonth(month: self.currentMonth, year: self.currentYear);
                tCollectionView.setMonth(month: next.month, year: next.year);
            }else{
                tCollectionView.setMonth(month: self.currentMonth, year: self.currentYear);
            }
        }
    }
    
    //MARK: - Month
    @IBAction func nextMonth(_ sender: Any) {
        self.scrollView.setContentOffset(CGPoint(x:self.frame.width * 2, y:0), animated: true);
    }
    @IBAction func lastMonth(_ sender: Any) {
        self.scrollView.setContentOffset(CGPoint(x:0, y:0), animated: true);
    }
    
    
}

//MARK: - UIScrollViewDelegate
extension JYCalendarView: UIScrollViewDelegate{
    public func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        self.didScroll(scrollView);
    }
    public func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
        self.didScroll(scrollView);
    }
    private func didScroll(_ scrollView: UIScrollView){
        let page = Int(scrollView.contentOffset.x / scrollView.frame.size.width);
        let tCollectionView = collectionViews[page];
        let current = tCollectionView.getCurrent();
        self.currentYear = current.year;
        self.currentMonth = current.month;

        self.dataSource?.calendarView(tCollectionView, year: self.currentYear, month: self.currentMonth);
        
        self.updateScrollView(page: page)
        self.updateCollectionMonth();
        self.setUp();
    }
}

//MARK: - JYCalendarCollectionDelegate
extension JYCalendarView: JYCalendarCollectionDelegate{
    func calendar(_ collectionView: JYCalendarCollection, didSelectItemAt calendarIndex: CalendarIndex) {
        if (calendarIndex.type == .lastMonth){
            self.scrollView.setContentOffset(
                CGPoint(x:0, y:0), animated: true);
        }
        if (calendarIndex.type == .nextMonth){
            self.scrollView.setContentOffset(
                CGPoint(x:self.frame.width * 2, y:0), animated: true);
        }
        print("day: \(calendarIndex.year)/\(calendarIndex.month)/\(calendarIndex.day)");
        
        
        if let delegate = self.delegate{
            if delegate.calendarView(collectionView, canSelectItemAt: calendarIndex){
                self.selectIndex(collectionView, calendarIndex: calendarIndex);
            }else{
                leftIndex = nil;
                rightIndex = nil;
                collectionView.reloadData();
            }
        }else{
            self.selectIndex(collectionView, calendarIndex: calendarIndex);
        }
    }
    
    func selectIndex(_ collectionView: JYCalendarCollection, calendarIndex: CalendarIndex){
        if justCanSelectOneDay{
            leftIndex = calendarIndex;
        }else{
            if leftIndex == nil{
                leftIndex = calendarIndex;
            } else if let leftIndex = leftIndex, rightIndex == nil, leftIndex.key != calendarIndex.key{
                rightIndex = calendarIndex;
                if let compareResult = leftIndex.compare(calendarIndex), compareResult == .orderedDescending{
                    self.rightIndex = leftIndex;
                    self.leftIndex = calendarIndex;
                }
            } else{
                leftIndex = nil;
                rightIndex = nil;
                leftIndex = calendarIndex;
            }
        }
        collectionView.reloadData();
    }
}

//MARK: - JYCalendarCollectionDataSource
extension JYCalendarView: JYCalendarCollectionDataSource{
    func calendar(_ collectionView: JYCalendarCollection, cellForItemAt calendarIndex: CalendarIndex) -> JYCalendarCell {
//
        guard let cell = self.dataSource?.calendarView(collectionView, cellForItemAt: calendarIndex) else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "JYCalendarCell", for: calendarIndex.indexPath) as! JYCalendarCell;
    
            if calendarIndex.type == .lastMonth{
                cell.setLastMonthDay(calendarIndex: calendarIndex);
            }else if calendarIndex.type == .nextMonth {
                cell.setNextMonthDay(calendarIndex: calendarIndex);
            }else{
                cell.setCurrentMonthDay(calendarIndex: calendarIndex);
                if Date.isHoliday(year: calendarIndex.year, month: calendarIndex.month, currentDay: calendarIndex.day){
                    cell.textLabel?.textColor = UIColor.red;
                }else if(Date.isToday(year: calendarIndex.year, month: calendarIndex.month, currentDay: calendarIndex.day)){
                    cell.textLabel?.textColor = UIColor.green;
                }
            }
    
            return cell;
        }
        return cell;
    }
    // 選取背景
    func calendar(_ collectionView: JYCalendarCollection, CellDayBgTypeForItemAt calendarIndex: CalendarIndex) -> CellDayBgType {
        if let leftIndex = leftIndex, leftIndex.key == calendarIndex.key, rightIndex == nil{
            return .Round;
        } else if let leftIndex = leftIndex, leftIndex.key == calendarIndex.key, rightIndex != nil{
            return .Left;
        } else if let rightIndex = rightIndex, rightIndex.key == calendarIndex.key{
            return .Right;
        } else if let leftIndex = leftIndex, let rightIndex = rightIndex, let leftCompareResult = leftIndex.compare(calendarIndex), leftCompareResult == .orderedAscending, let rightCompareResult = rightIndex.compare(calendarIndex), rightCompareResult == .orderedDescending{
            return .Center;
        }

        return .Hide;
    }
}


