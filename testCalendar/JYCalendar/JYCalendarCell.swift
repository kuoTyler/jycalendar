//
//  JYCalendarCell.swift
//  testCalendar
//
//  Created by baytony on 2018/12/11.
//  Copyright © 2018 tylerkuo. All rights reserved.
//

import UIKit

open class JYCalendarCell: UICollectionViewCell {
    public var textLabel: UILabel?
//    public var selectedBg: UIView?;
    private var bgType: CellDayBgType = .Hide;
    
    override public init(frame: CGRect) {
        super.init(frame: frame.integral);
        self.onCreate();
    }
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder);
        self.onCreate();
    }
    
    private func onCreate(){
        self.contentView.backgroundColor = UIColor.init(red: 53.0/255.0, green: 121.0/255.0, blue: 246.0/255.0, alpha: 0.9);
        self.layer.masksToBounds = true;
//        let selectedBg = UIView(frame: self.frame.integral);
//        selectedBg.backgroundColor = UIColor.init(red: 53.0/255.0, green: 121.0/255.0, blue: 246.0/255.0, alpha: 0.9);
//        self.addSubview(selectedBg);
//        selectedBg.isHidden = true;
//        self.selectedBg = selectedBg;
        
        let textLabel = UILabel.init();
        textLabel.font = UIFont.systemFont(ofSize: 17);
        textLabel.textAlignment = .center;
        self.addSubview(textLabel);
        self.textLabel = textLabel;
        
//        selectedBg.translatesAutoresizingMaskIntoConstraints = false;
//        var selectedBgConstraints = [NSLayoutConstraint]();
//        selectedBgConstraints.append(selectedBg.topAnchor.constraint(equalTo: self.topAnchor));
//        selectedBgConstraints.append(selectedBg.bottomAnchor.constraint(equalTo: self.bottomAnchor));
//        selectedBgConstraints.append(selectedBg.leftAnchor.constraint(equalTo: self.leftAnchor));
//        selectedBgConstraints.append(selectedBg.rightAnchor.constraint(equalTo: self.rightAnchor));
//        NSLayoutConstraint.activate(selectedBgConstraints);
        
        textLabel.translatesAutoresizingMaskIntoConstraints = false;
        var textLabelConstraints = [NSLayoutConstraint]();
        textLabelConstraints.append(textLabel.topAnchor.constraint(greaterThanOrEqualTo: self.topAnchor));
        textLabelConstraints.append(textLabel.bottomAnchor.constraint(lessThanOrEqualTo: self.bottomAnchor));
        textLabelConstraints.append(textLabel.leftAnchor.constraint(greaterThanOrEqualTo: self.leftAnchor));
        textLabelConstraints.append(textLabel.rightAnchor.constraint(lessThanOrEqualTo: self.rightAnchor));
        textLabelConstraints.append(textLabel.centerXAnchor.constraint(equalTo: self.centerXAnchor));
        textLabelConstraints.append(textLabel.centerYAnchor.constraint(equalTo: self.centerYAnchor));
        NSLayoutConstraint.activate(textLabelConstraints);
    }
    
    func setSelect(bgType: CellDayBgType = .Hide){
        self.bgType = bgType;
        /*
            Hide;      //隱藏
            Round;     //被選中的日期，完整圓
            Left;      //被選中的日期，左側圓角
            Center;    //被選中的日期，不切圓角
            Right;     //被選中的日期，右側圓角
            */
//        selectedBg?.isHidden = false;
//        if self.bgType == .Left{
//            selectedBg?.setCornerRadius(17.5, with: .TopLeftBottomLeft);
//        }else if self.bgType == .Right{
//            selectedBg?.setCornerRadius(17.5, with: .TopRightBottomRight);
//        }else if self.bgType == .Center{
//            selectedBg?.setCornerRadius(0, with: .Default);
//        }else if self.bgType == .Round{
//            selectedBg?.setCornerRadius(17.5, with: .Default);
//        }else {
//            selectedBg?.isHidden = true;
//        }
        
        self.contentView.backgroundColor = UIColor.init(red: 53.0/255.0, green: 121.0/255.0, blue: 246.0/255.0, alpha: 0.9);
        if self.bgType == .Left{
            self.contentView.setCornerRadius(17.5, with: .TopLeftBottomLeft);
        }else if self.bgType == .Right{
            self.contentView.setCornerRadius(17.5, with: .TopRightBottomRight);
        }else if self.bgType == .Center{
            self.contentView.setCornerRadius(0, with: .Default);
            self.contentView.backgroundColor = UIColor.init(red: 53.0/255.0, green: 121.0/255.0, blue: 246.0/255.0, alpha: 0.5);
        }else if self.bgType == .Round{
            self.contentView.setCornerRadius(17.5, with: .Default);
        }else {
            self.contentView.backgroundColor = UIColor.white;
        }
    }
    
    internal func setCurrentMonthDay(calendarIndex: CalendarIndex){
        self.textLabel?.textColor = UIColor.black;
        self.textLabel?.text = "\(calendarIndex.day)";
    }
    internal func setNextMonthDay(calendarIndex: CalendarIndex){
        self.textLabel?.textColor = UIColor.gray;
        self.textLabel?.text = "\(calendarIndex.day)";
    }
    internal func setLastMonthDay(calendarIndex: CalendarIndex){
        self.textLabel?.textColor = UIColor.gray;
        self.textLabel?.text = "\(calendarIndex.day)";
    }
    
    public override func layoutSubviews() {
        super.layoutSubviews();
    }
}

extension UIView{
    enum CornerRadiusViewType {
        case Default;
        case TopLeft;
        case TopRight;
        case BottomLeft;
        case BottomRight;
        case TopLeftTopRight;
        case TopLeftBottomLeft;
        case TopLeftBottomRight;
        case TopRightBottomLeft;
        case TopRightBottomRight;
        case BottomLeftBottomRight;
        case TopLeftTopRightBottomLeft;
        case TopLeftTopRightBottomRight;
        case TopLeftBottomLeftBottomRight;
        case TopRightBottomLeftBottomRight;
    }
    
    
    func setCornerRadius(_ cornerRadius: CGFloat, with type: CornerRadiusViewType){
        var maskPath: UIBezierPath = UIBezierPath();
        switch type {
        case .Default:
            maskPath = UIBezierPath.init(roundedRect: self.bounds, byRoundingCorners: .allCorners, cornerRadii: CGSize(width: cornerRadius, height: cornerRadius));
            break;
        case .TopLeft:
            maskPath = UIBezierPath.init(roundedRect: self.bounds, byRoundingCorners: .topLeft, cornerRadii: CGSize(width: cornerRadius, height: cornerRadius));
            break;
        case .TopRight:
            maskPath = UIBezierPath.init(roundedRect: self.bounds, byRoundingCorners: .topRight, cornerRadii: CGSize(width: cornerRadius, height: cornerRadius));
            break;
        case .BottomLeft:
            maskPath = UIBezierPath.init(roundedRect: self.bounds, byRoundingCorners: .bottomLeft, cornerRadii: CGSize(width: cornerRadius, height: cornerRadius));
            break;
        case .BottomRight:
            maskPath = UIBezierPath.init(roundedRect: self.bounds, byRoundingCorners: .bottomRight, cornerRadii: CGSize(width: cornerRadius, height: cornerRadius));
            break;
        case .TopLeftTopRight:
            maskPath = UIBezierPath.init(roundedRect: self.bounds, byRoundingCorners: [.topLeft, .topRight], cornerRadii: CGSize(width: cornerRadius, height: cornerRadius));
            break;
        case .TopLeftBottomLeft:
            maskPath = UIBezierPath.init(roundedRect: self.bounds, byRoundingCorners: [.topLeft, .bottomLeft], cornerRadii: CGSize(width: cornerRadius, height: cornerRadius));
            break;
        case .TopLeftBottomRight:
            maskPath = UIBezierPath.init(roundedRect: self.bounds, byRoundingCorners: [.topLeft, .bottomRight], cornerRadii: CGSize(width: cornerRadius, height: cornerRadius));
            break;
        case .TopRightBottomLeft:
            maskPath = UIBezierPath.init(roundedRect: self.bounds, byRoundingCorners: [.topRight, .bottomLeft], cornerRadii: CGSize(width: cornerRadius, height: cornerRadius));
            break;
        case .TopRightBottomRight:
            maskPath = UIBezierPath.init(roundedRect: self.bounds, byRoundingCorners: [.topRight, .bottomRight], cornerRadii: CGSize(width: cornerRadius, height: cornerRadius));
            break;
        case .BottomLeftBottomRight:
            maskPath = UIBezierPath.init(roundedRect: self.bounds, byRoundingCorners: [.bottomLeft, .bottomRight], cornerRadii: CGSize(width: cornerRadius, height: cornerRadius));
            break;
        case .TopLeftTopRightBottomLeft:
            maskPath = UIBezierPath.init(roundedRect: self.bounds, byRoundingCorners: [.topLeft, .topRight, .bottomLeft], cornerRadii: CGSize(width: cornerRadius, height: cornerRadius));
            break;
        case .TopLeftTopRightBottomRight:
            maskPath = UIBezierPath.init(roundedRect: self.bounds, byRoundingCorners: [.topLeft, .topRight, .bottomRight], cornerRadii: CGSize(width: cornerRadius, height: cornerRadius));
            break;
        case .TopLeftBottomLeftBottomRight:
            maskPath = UIBezierPath.init(roundedRect: self.bounds, byRoundingCorners: [.topLeft, .bottomLeft, .bottomRight], cornerRadii: CGSize(width: cornerRadius, height: cornerRadius));
            break;
        case .TopRightBottomLeftBottomRight:
            maskPath = UIBezierPath.init(roundedRect: self.bounds, byRoundingCorners: [.topRight, .bottomLeft, .bottomRight], cornerRadii: CGSize(width: cornerRadius, height: cornerRadius));
            break;
        }
        
        let maskLayer = CAShapeLayer();
        maskLayer.frame = self.bounds;
        maskLayer.path = maskPath.cgPath;
        self.layer.mask = maskLayer;
    }
}
