//
//  JYCalendarComponent.swift
//  testCalendar
//
//  Created by baytony on 2018/12/11.
//  Copyright © 2018 tylerkuo. All rights reserved.
//

import UIKit

public enum CalendarType {
    case currentMonth;
    case lastMonth;
    case nextMonth;
}

public struct CalendarIndex {
    public var year: Int;
    public var month: Int;
    public var day: Int;
    public var indexPath: IndexPath;
    
    public var type: CalendarType;
    
    public var key: String{
        return String.init(format: "%04d%02d%02d", year,month,day);
    }
    
    public func compare(_ otherIndex: CalendarIndex) -> ComparisonResult?{
        let formate = DateFormatter();
        formate.dateFormat = "yyyyMMdd";
        if let date = formate.date(from: self.key) ,let otherDate = formate.date(from: otherIndex.key){
            return date.compare(otherDate);
        }else{
            return nil;
        }
    }
}

open class JYCalendarComponent: NSObject {
    
    static public func getLastMonth(month: Int, year: Int) -> (month: Int, year: Int){
        var tMonth = month;
        var tYear = year;
        tMonth -= 1;
        if (tMonth == 0){
            tMonth = 12;
            tYear -= 1;
        }
        return (tMonth, tYear);
    }
    
    static public func getNextMonth(month: Int, year: Int) -> (month: Int, year: Int){
        var tMonth = month;
        var tYear = year;
        tMonth += 1;
        if (tMonth == 13){
            tMonth = 1;
            tYear += 1;
        }
        return (tMonth, tYear);
    }
}
