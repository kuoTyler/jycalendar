//
//  Array+Extension.swift
//  POS
//
//  Created by Tyler on 2018/11/3.
//  Copyright © 2018 tylerkuo. All rights reserved.
//

import Foundation

extension Array {
    public func toDictionary<Key: Hashable>(with selectKey: (Element) -> Key) -> [Key:Element] {
        var dict = [Key:Element]()
        for element in self {
            dict[selectKey(element)] = element
        }
        return dict
    }
}
