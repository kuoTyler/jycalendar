//
//  ViewController.swift
//  testCalendar
//
//  Created by baytony on 2018/12/5.
//  Copyright © 2018 tylerkuo. All rights reserved.
//

import UIKit
import SnapKit
import SwiftDate
import PromiseKit
import GoogleSignIn

class ViewController: UIViewController, JYCalendarDataSource, JYCalendarDelegete {

    

    var calendar: JYCalendarView?
    
    var manager: GoogleManager = GoogleManager();
    
    var events: [String: [GoogleCalendarObject]] = [:];
    
    var tempEvents:[GoogleCalendarObject] = [];
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.onCreate();

    }

    func onCreate(){
        self.calendar = JYCalendarView.loadViewFromNib();
        self.calendar?.register(TestCollectionViewCell.self, forCellWithReuseIdentifier: "CalendarViewCell");
        self.calendar?.dataSource = self;
        self.calendar?.delegate = self;
        self.calendar?.setSelectType(oneDay: false);
        self.view.addSubview(self.calendar!);
        
        self.calendar?.snp.makeConstraints({ (make) in
            if #available(iOS 11.0, *) {
                make.top.equalTo(self.view.safeAreaLayoutGuide)
//                make.bottom.equalTo(self.view.safeAreaLayoutGuide);
            } else {
                make.top.equalTo(self.topLayoutGuide.snp.bottom);
//                make.bottom.equalTo(self.bottomLayoutGuide.snp.top);
            };
            make.left.right.equalTo(self.view);
            make.height.greaterThanOrEqualTo(400);
//            make.width.equalTo(371);
        })
    }

    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews();
//        self.calendar?.layoutIfNeeded();
    }
    
    //MARK: - JYCalendarDataSource
    func calendarView(_ collectionView: JYCalendarCollection, cellForItemAt calendarIndex: CalendarIndex) -> JYCalendarCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CalendarViewCell", for: calendarIndex.indexPath) as! TestCollectionViewCell;
        
        if calendarIndex.month == collectionView.getCurrent().month{
            cell.dayLable.textColor = UIColor.black;
            cell.dayLable.text = String("\(calendarIndex.day)");
        }else{
            cell.dayLable.textColor = UIColor.lightGray;
            cell.dayLable.text = String("\(calendarIndex.month)/\(calendarIndex.day)");
        }
        
        cell.setDelDay(isDel: arc4random() % 2 == 0);
        
//        if calendarIndex.type == .lastMonth{
//            cell.setLastMonthDay(calendarIndex: calendarIndex);
//        }else if calendarIndex.type == .nextMonth {
//            cell.setNextMonthDay(calendarIndex: calendarIndex);
//        }else{
//            cell.setCurrentMonthDay(calendarIndex: calendarIndex);
//            if Date.isHoliday(year: calendarIndex.year, month: calendarIndex.month, currentDay: calendarIndex.day){
//                cell.textLabel?.textColor = UIColor.red;
//            }else if(Date.isToday(year: calendarIndex.year, month: calendarIndex.month, currentDay: calendarIndex.day)){
//                cell.textLabel?.textColor = UIColor.green;
//            }
//        }

        return cell;
    }
    
    func calendarView(_ collectionView: JYCalendarCollection, year: Int, month: Int) {
        let firstDay = DateInRegion.init(year: year, month: month, day: 1, region: Region.ISO).dateAtStartOf(.month);
        let lastDay = DateInRegion.init(year: year, month: month, day: 1, region: Region.ISO).dateAtEndOf(.month);
        
        
    }
    
    //MARK: - JYCalendarDelegete
    func calendarView(_ collectionView: JYCalendarCollection, canSelectItemAt calendarIndex: CalendarIndex) -> Bool {
        if calendarIndex.year == 2020, calendarIndex.month == 7, calendarIndex.day == 15{
            return false;
        }
        return true;
    }
}

